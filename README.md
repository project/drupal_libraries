# Composer Repository with Drupal Libraries

## What is this project and how it will help me
Drupal libraries e.g. CKEditor plugins are not listed on Packagist neither on Drupal.org Packagist. So if you use composer in your project it is very likely that you have defined repositories for different libraries in your composer.json file like I had:
```
       {
            "type": "package",
            "package": {
                "name": "ckeditor/tabletoolstoolbar",
                "version": "0.0.1",
                "type": "drupal-library",
                "dist": {
                    "url": "https://download.ckeditor.com/tabletoolstoolbar/releases/tabletoolstoolbar_0.0.1.zip",
                    "type": "zip"
                }
            }
        }
```
Thanks to the Drupal Libraries repository created by the Drupal community, this is no longer necessary.

(Browse repository)[https://petabajt.gitlab.io/drupal-libraries/]. It's hosted on the GitLab Pages.

## How to start
Add this Composer repository to your project's composer.json file, then you can require these private packages just like you would with one from Packagist.

```
{
  "repositories": [{
    "type": "composer",
    "url": "https://petabajt.gitlab.io/drupal-libraries"
  }]
}
```

and start adding dependencies as usual:
```
composer require drupal_ckeditor/tabletoolstoolbar
```

## There is no library?
I will be happy to add new libraries to the repository. Each library should contain:

Name which follows convention e.g. drupal_ckeditor/name_of_plugin
Version
License
Homepage
The URL to download the library, preferably zip
